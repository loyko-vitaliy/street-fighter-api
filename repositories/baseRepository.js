const { dbAdapter } = require('../config/db')
const { v4 } = require('uuid')
const NotFoundError = require('../utils/NotFoundError')

class BaseRepository {
  constructor(collectionName) {
    this.dbContext = dbAdapter.get(collectionName)
    this.collectionName = collectionName
  }

  generateId() {
    return v4()
  }

  getAll() {
    return this.dbContext.value()
  }

  getOne(search) {
    const resource = this.dbContext.find(search).value()

    if (!resource) {
      throw new NotFoundError()
    }

    return resource
  }

  create(data) {
    data.id = this.generateId()
    data.createdAt = new Date()
    const list = this.dbContext.push(data).write()
    return list.find((it) => it.id === data.id)
  }

  update(id, dataToUpdate) {
    const resource = this.getOne({ id })

    if (!resource) {
      throw new NotFoundError()
    }

    dataToUpdate.updatedAt = new Date()
    return this.dbContext.find({ id }).assign(dataToUpdate).write()
  }

  delete(id) {
    const resource = this.getOne({ id })

    if (!resource) {
      throw new NotFoundError()
    }

    return this.dbContext.remove({ id }).write()
  }
}

exports.BaseRepository = BaseRepository
