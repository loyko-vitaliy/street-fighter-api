const { UserRepository } = require('../repositories/userRepository')

class UserService {
  getUsers() {
    return UserRepository.getAll()
  }

  getUser(property) {
    return UserRepository.getOne(property)
  }

  createUser(user) {
    return UserRepository.create(user)
  }

  updateUser(id, user) {
    return UserRepository.update(id, user)
  }

  deleteUser(id) {
    return UserRepository.delete(id)
  }

  // search(search) {
  //   const item = UserRepository.getOne(search)
  //   if (!item) {
  //     return null
  //   }
  //   return item
  // }
}

module.exports = new UserService()
