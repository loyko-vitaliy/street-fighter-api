const { FighterRepository } = require('../repositories/fighterRepository')

class FighterService {
  getFighters() {
    return FighterRepository.getAll()
  }

  getFighter(property) {
    return FighterRepository.getOne(property)
  }

  createFighter(fighter) {
    return FighterRepository.create(fighter)
  }

  updateFighter(id, fighter) {
    return FighterRepository.update(id, fighter)
  }

  deleteFighter(id) {
    return FighterRepository.delete(id)
  }
}

module.exports = new FighterService()
