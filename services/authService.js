const UserService = require('./userService')

class AuthService {
  login(userData) {
    return UserService.getUser(userData)
  }
}

module.exports = new AuthService()
