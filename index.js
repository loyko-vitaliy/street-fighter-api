const path = require('path')
const express = require('express')
const cors = require('cors')
require('./config/db')
const { notFoundMiddleware } = require('./middlewares/not-found.error.middleware')
const { errorHandlingMiddleware } = require('./middlewares/error.handling.middleware')

const app = express()

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const routes = require('./routes/index')
routes(app)

app.use('/', express.static(path.join(__dirname, 'client/build')))

app.use(notFoundMiddleware)
app.use(errorHandlingMiddleware)

const port = process.env.PORT || 3050
app.listen(port, () => {})

exports.app = app
