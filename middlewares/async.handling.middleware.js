exports.asyncHandler = (middleware) => (req, res, next) => {
  try {
    res.data = middleware(req, res, next)
  } catch (err) {
    next(err)
  }

  next()
}
