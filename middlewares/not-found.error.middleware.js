exports.notFoundMiddleware = (req, res, next) => {
  res.status(404).json({
    error: true,
    message: 'Unable to find the requested resource!'
  })
  next()
}
