const { user } = require('../models/user')
const UserService = require('../services/userService')
const Validator = require('../utils/Validator')

const validate = (id, body, next, { required = true } = {}) => {
  const properties = Object.keys(user).filter((field) => field !== 'id')
  const validator = new Validator({ properties, id, body, service: UserService })

  validator.checkAdditionalProperties()
  required && validator.checkRequiredProperties()
  validator.checkPropertyPattern({ property: 'phoneNumber', pattern: /^\+380[0-9]{9}$/ })
  validator.checkPropertyPattern({ property: 'email', pattern: /^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/ })
  validator.checkString({ property: 'password', min: 3, max: 50 })
  validator.checkString({ property: 'firstName', min: 3, max: 60 })
  validator.checkString({ property: 'lastName', min: 3, max: 60 })
  validator.checkUnique({ properties: ['phoneNumber', 'email'] })

  validator.isValid ? next() : next(validator.error)
}

const createUserValid = ({ body, params: { id } }, res, next) => {
  // TODO: Implement validatior for user entity during creation
  validate(id, body, next)
}

const updateUserValid = ({ body, params: { id } }, res, next) => {
  // TODO: Implement validatior for user entity during update
  validate(id, body, next, { required: false })
}

exports.createUserValid = createUserValid
exports.updateUserValid = updateUserValid
