const { user } = require('../models/user')
const UserService = require('../services/userService')
const Validator = require('../utils/Validator')

exports.loginUserValid = ({ body }, res, next) => {
  const properties = Object.keys(user).filter((field) => field === 'email' || field === 'password')
  const validator = new Validator({ properties, body, service: UserService })

  validator.checkAdditionalProperties()
  validator.checkRequiredProperties()
  validator.checkPropertyPattern({ property: 'email', pattern: /^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/ })
  validator.checkString({ property: 'password', min: 3, max: 50 })

  validator.isValid ? next() : next(validator.error)
}
