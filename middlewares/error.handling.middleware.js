const NotFoundError = require('../utils/NotFoundError')

exports.errorHandlingMiddleware = (err, req, res, next) => {
  const errorResponse = { error: true, message: err.message }
  let status = 400

  if (err.details) {
    errorResponse.details = err.details
  }

  if (err instanceof NotFoundError) {
    status = 404
  }

  res.status(status).json(errorResponse)
}
