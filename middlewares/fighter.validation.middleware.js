const { fighter } = require('../models/fighter')
const FighterService = require('../services/fighterService')
const Validator = require('../utils/Validator')

const validate = (id, body, next, { required = true } = {}) => {
  const properties = Object.keys(fighter).filter((field) => field !== 'id')
  const validator = new Validator({ properties, id, body, service: FighterService })

  validator.checkAdditionalProperties()
  required && validator.checkRequiredProperties()
  validator.checkString({ property: 'name', min: 3, max: 50 })
  validator.checkNumber({ property: 'health', min: 20, max: 500 })
  validator.checkNumber({ property: 'power', min: 1, max: 100 })
  validator.checkNumber({ property: 'defense', min: 1, max: 10 })
  validator.checkUnique({ properties: ['name'] })

  validator.isValid ? next() : next(validator.error)
}

const createFighterValid = ({ body, params: { id } }, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  validate(id, body, next)
}

const updateFighterValid = ({ body, params: { id } }, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  validate(id, body, next, { required: false })
}

exports.createFighterValid = createFighterValid
exports.updateFighterValid = updateFighterValid
