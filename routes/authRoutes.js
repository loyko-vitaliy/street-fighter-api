const { Router } = require('express')
const AuthService = require('../services/authService')
const { responseMiddleware } = require('../middlewares/response.middleware')
const { asyncHandler } = require('../middlewares/async.handling.middleware')
const { loginUserValid } = require('../middlewares/auth.validation.middleware')

const router = Router()

router.post(
  '/login',
  loginUserValid,
  asyncHandler(({ body }, res, next) => AuthService.login(body)),
  responseMiddleware
)

module.exports = router
