const { Router } = require('express')
const FighterService = require('../services/fighterService')
const { asyncHandler } = require('../middlewares/async.handling.middleware')
const { responseMiddleware } = require('../middlewares/response.middleware')
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware')

const router = Router()

router.get(
  '/',
  asyncHandler((req, res, next) => FighterService.getFighters()),
  responseMiddleware
)

router.post(
  '/',
  createFighterValid,
  asyncHandler(({ body }, res, next) => FighterService.createFighter(body)),
  responseMiddleware
)

router.get(
  '/:id',
  asyncHandler(({ params: { id } }, res, next) => FighterService.getFighter({ id })),
  responseMiddleware
)

router.put(
  '/:id',
  updateFighterValid,
  asyncHandler(({ params: { id }, body }, res, next) => FighterService.updateFighter(id, body)),
  responseMiddleware
)

router.delete(
  '/:id',
  asyncHandler(({ params: { id } }, res, next) => FighterService.deleteFighter(id)),
  responseMiddleware
)

module.exports = router
