const { Router } = require('express')
const UserService = require('../services/userService')
const { asyncHandler } = require('../middlewares/async.handling.middleware')
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware')
const { responseMiddleware } = require('../middlewares/response.middleware')

const router = Router()

router.get(
  '/',
  asyncHandler((req, res, next) => UserService.getUsers()),
  responseMiddleware
)

router.post(
  '/',
  createUserValid,
  asyncHandler(({ body }, res, next) => UserService.createUser(body)),
  responseMiddleware
)

router.get(
  '/:id',
  asyncHandler(({ params: { id } }, res, next) => UserService.getUser({ id })),
  responseMiddleware
)

router.put(
  '/:id',
  updateUserValid,
  asyncHandler(({ params: { id }, body }, res, next) => UserService.updateUser(id, body)),
  responseMiddleware
)

router.delete(
  '/:id',
  asyncHandler(({ params: { id } }, res, next) => UserService.deleteUser(id)),
  responseMiddleware
)

module.exports = router
