const NotFoundError = require('./NotFoundError')

class Validator {
  constructor({ properties, id = null, body, service } = {}) {
    this.service = service
    this.properties = properties
    this.id = id
    this.body = body
    this.error = this._initError()
  }

  get isValid() {
    return this.error.details.length === 0
  }

  checkAdditionalProperties() {
    for (const property in this.body) {
      if (!this.properties.includes(property)) {
        this.error.details.push(
          this._createError({ type: 'Additional property', message: `Property ${property} is not allowed`, property })
        )
      }
    }
  }

  checkRequiredProperties() {
    for (const property of this.properties) {
      if (!this.body[property]) {
        this.error.details.push(
          this._createError({ type: 'Required property', message: `Property ${property} is required`, property })
        )
      }
    }
  }

  checkPropertyPattern({ property, pattern }) {
    const value = this.body[property]

    if (value && !pattern.test(value)) {
      this.error.details.push(
        this._createError({ type: 'Wrong Format', message: `Wrong format for ${property}`, property })
      )
    }
  }

  checkString({ property, min = 1, max = 100 }) {
    const value = this.body[property]

    if (value) {
      const error = { type: 'String Validation', property }

      if (typeof value !== 'string') {
        error.message = `Property ${property} must be a string`
      } else if (value.length < min || value.length > max) {
        error.message = `Property ${property} must contains at least ${min} or at most ${max} symbols`
      }

      error.message && this.error.details.push(error)
    }
  }

  checkNumber({ property, min = 0, max = 100 }) {
    const value = Number(this.body[property])

    if (value) {
      const error = { type: 'Number Validation', property }

      if (typeof value !== 'number') {
        error.message = `Property ${property} must be a number`
      } else if (value < min || value > max) {
        error.message = `Property ${property} must be greater than ${min} and less than ${max}`
      }

      error.message && this.error.details.push(error)
    }
  }

  checkUnique({ properties }) {
    for (const property of properties) {
      try {
        const resource = this.service[this._getResource()]({ [property]: this.body[property] })

        if (resource && this.id !== resource.id) {
          this.error.details.push({
            type: 'Unique Validation',
            property,
            message: `Property ${property} must be unique`
          })
        }
      } catch (err) {
        if (!err instanceof NotFoundError) {
          throw err
        }
      }
    }
  }

  _getResource() {
    const [, resource] = this.service.constructor.name.match(/^(.+)Service$/)
    return `get${resource}`
  }

  _initError() {
    const error = new Error('Model Validation Failed')
    error.details = []
    return error
  }

  _createError({ type, message, property }) {
    return { type, message, property }
  }
}

module.exports = Validator
