class NotFoundError extends Error {
  constructor(message = 'Resource Not Found') {
    super(message)
  }
}

module.exports = NotFoundError
